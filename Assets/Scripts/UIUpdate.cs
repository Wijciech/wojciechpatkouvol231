﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUpdate : MonoBehaviour
{
    [SerializeField]
    private Text _towersText;


    void Update()
    {
        _towersText.text = "Towers: "+ GameObject.FindGameObjectsWithTag("Tower").Length;
    }
}
