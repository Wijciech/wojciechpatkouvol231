﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tower : MonoBehaviour
{
    [SerializeField]
    private GameObject _bulletPrefab;

    private SpriteRenderer _spriteRenderer;

    private int _towerLifeTime;


    private void Awake()
    {
       _spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        StartCoroutine(Rotate());
    }

    IEnumerator AwakeTowers()
    {
        yield return new WaitForSeconds(6f);
        gameObject.SetActive(true);
    }

    IEnumerator Rotate()
    {
        if (_towerLifeTime < 12)
        {
            _spriteRenderer.color = new Color(255, 0, 0);
            transform.Rotate(0, 0, Random.Range(15, 45));
            yield return new WaitForSeconds(0.5f);
            Fire();
            _towerLifeTime++;
            StartCoroutine(Rotate());
        }
        else
        {
            _spriteRenderer.color = new Color(255, 255, 255);
        }

    }

    private void Fire()
    {

        var _bulletInstance = Instantiate(_bulletPrefab, transform.position, Quaternion.identity);

    }
}
