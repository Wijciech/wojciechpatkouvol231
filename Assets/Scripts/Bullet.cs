﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float _bulletSpeed = 4f;

    [SerializeField]
    private Rigidbody2D _bulletBody;

    [SerializeField]
    private GameObject _towerPrefab;

    void FixedUpdate()
    {
        _bulletBody.AddForce(Vector2.up * _bulletSpeed, ForceMode2D.Impulse);
        StartCoroutine(SpawnTower());
    }

    IEnumerator SpawnTower()
    {
        yield return new WaitForSeconds(Random.Range(1f,4f));
        Destroy(this.gameObject);
        var _tower = Instantiate(_towerPrefab, transform.position, Quaternion.identity);
        _tower.SetActive(false);

     }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Tower")
        {
            Destroy(this.gameObject);
            Destroy(collider);
        }
    }
}
